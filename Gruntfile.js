const watched = ['index.js', 'src/**/*.js', 'test/**.spec.js'];

function initializer(grunt) {
	grunt.initConfig({
		watch: {
			files: watched,
			tasks: ['mochaTest'],
		},
		mochaTest: {
			lib: {
				options: {
					reporter: 'spec',
				},
				src: ['test/**/*.spec.js'],
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-mocha-test');

	grunt.registerTask('default', ['watch']);
}

module.exports = initializer;