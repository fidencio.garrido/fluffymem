# [2.0.0] - 2021-01-07
## Changed
- `miss_expired` event is fired instead of `miss` for items that are still in 
the cache but beyond the expired timestamp

# [1.2.0] - 2019-02-24
## Added
- Option to do full storage cleanup for faster cycles