const { Cache } = require('..');

const cacheQuick = new Cache({
	limit: 100,
	cleanupInterval: 60,
	ttl: 120,
});

const cacheLarge = new Cache({
	limit: 800,
	cleanupInterval: 30,
	ttl: 25,
});

const customTTL = new Cache({
	limit: 20,
	cleanupInterval: 30,
	ttl: (id, item) => {
		if (id < 200000) {
			return 3;
		}
		return 50;
	},
});

let lastQuick = Date.now();
let lastLarge = Date.now();

cacheQuick.on('gc', () => {
	const took = Date.now() - lastQuick;
	console.log('quick cleared', took);
	lastQuick = Date.now();
});

cacheLarge.on('gc', () => {
	const took = Date.now() - lastLarge;
	console.log('large cleared', took);
	lastLarge = Date.now();
});


customTTL.on('hit', () => {
	console.log('\n\ncool :)\n\n');
});

function randomNumber(min, max) {
	return Math.floor(Math.random() * Math.floor(max));
}

const populator = setInterval(() => {
	const id = randomNumber(1, 150);
	customTTL.get(id);
	cacheQuick.put(id, 'Fluffy');
	cacheLarge.put(id, 'Fluffy');
	customTTL.put(id, 'Fluffy');
	console.log(cacheLarge.size(), cacheQuick.size(), customTTL.size());
	if (cacheQuick.size() > 100) {
		throw 'Error quick';
	}
	if (cacheLarge.size() > 20000) {
		throw 'Error large';
	}
}, 50);