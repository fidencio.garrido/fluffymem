const expect = require('chai').expect;

const { Cache } = require('..');

const configs = {
	good: {
		autoRefresh: true,
		limit: 1000,
		format: 'literal', // buffer
		cleanupInterval: 2, // seconds
		handlesLock: 40,
		unless: (input) => {
			return input === 'Fido';
		},
		ttl: 1, // seconds or function
	},
	inclusive: {
		autoRefresh: true,
		limit: 1000,
		format: 'literal', // buffer
		cleanupInterval: 2, // seconds
		handlesLock: 40,
		onlyif: (input) => {
			return input === 'Fluffy';
		},
		ttl: 1, // seconds or function
	},
	fullCleanup: {
		autoRefresh: true,
		limit: 1000,
		fullCleanup: true,
		cleanupInterval: 1, // seconds
		handlesLock: 40,
		ttl: 10, // seconds or function
	}
};

describe('Given a cache object', () => {
	const cache = new Cache(configs.good);
	let hit = 0;
	let miss = 0;
	cache.on('hit', () => {
		hit += 1;
	});
	cache.on('miss', () => {
		miss += 1;
	});

	it('should store an object and return it', () => {
		const fluffy = 'Fluffy';
		cache.put(1, fluffy);
		const val = cache.get(1);
		expect(val).to.equal(fluffy);
		expect(cache.size()).to.equal(1);
	});

	it('should report cache hits', () => {
		expect(hit).to.equal(1);
		expect(miss).to.equal(0);
	});

	it('should not store an object if it fails the unless test', () => {
		cache.put(2, 'Fido');
		const val = cache.get(2);
		expect(val).to.equal(null);
		expect(cache.size()).to.equal(1);
	});

	it('should report cache miss', () => {
		expect(hit).to.equal(1);
		expect(miss).to.equal(1);
	});

	it('should not remove the item before expiration', (done) => {
		setTimeout(() => {
			expect(cache.get(1)).to.equal('Fluffy');
			expect(cache.size()).to.equal(1);
			done();
		}, 100);
	});

	it('should remove the item upon expiration', (done) => {
		const time = (configs.good.ttl * 1000) + 10;
		setTimeout(() => {
			expect(cache.get(1)).to.equal(null);
			expect(cache.size()).to.equal(0);
			done();
		}, time);
	});
});


describe('A limited cache', () => {
	const limitedCache = new Cache({
		limit: 2,
	});
	it('should now allow to store more items beyond the limit', () => {
		limitedCache.put(1, 'Fido');
		limitedCache.put(2, 'Fluffy');
		limitedCache.put(3, 'Tibbers');
		limitedCache.put(4, 'Picky');
		expect(limitedCache.size()).to.equal(2);
	});
});

describe('Given an inclusive cache', ()=> {
	const cache = new Cache(configs.inclusive);

	it('should not store an object if it fails the onlyif test', () => {
		cache.put(2, 'Fido');
		const val = cache.get(2);
		expect(val).to.equal(null);
		expect(cache.size()).to.equal(0);
	});

	it('should store an object if it passes the onlyif test', () => {
		cache.put(1, 'Fluffy');
		const val = cache.get(1);
		expect(val).to.equal('Fluffy');
		expect(cache.size()).to.equal(1);
	});
});

describe('Given a full disposable cache', () => {
	it('should remove everything on GC', (done) => {
		const cache = new Cache(configs.fullCleanup);
		cache.put(1, 'fido');
		expect(cache.size()).to.equal(1);
		setTimeout(() => {
			expect(cache.size()).to.equal(0);
			done();
		}, 1010);
	});
});