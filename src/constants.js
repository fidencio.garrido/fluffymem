const LIB_EVENTS = {
	MISS: 'miss',
	MISS_EXPIRED: 'miss_expired',
	HIT: 'hit',
	BAD_CONFIG: 'bad_configuration',
	GC: 'gc',
};

exports.LIB_EVENTS = LIB_EVENTS;