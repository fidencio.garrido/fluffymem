const EventEmitter = require('events');
const constants = require('./src/constants');

const { LIB_EVENTS } = constants;

const defaultConfiguration = {
	limit: 2000, // max number of items to keep
	format: 'literal', //
	cleanupInterval: 60, // seconds for gc execution
	// if the number of node handles are higher than this, it will skip gc for
	// up to two times
	handlesLock: 40,
	ttl: 60, // document expiry in seconds
	unless: () => false, // function to discard elements to put in the cache
	// if true, it won't evaluate TTL for GC but rather will clean everything up
	fullCleanup: false,
	// reverse unless, it will storage only those elements who return true
	onlyif: null,
};

class Cache extends EventEmitter {
	constructor(options) {
		super();
		const configuration = { ...defaultConfiguration, ...options };
		if (Cache.isValidConfiguration(configuration)) {
			const adjustedConfig = Cache._adjustSettings(configuration);
			this.store = new Map();
			this.unless = adjustedConfig.unless;
			this.limit = adjustedConfig.limit;
			this.handlesLock = adjustedConfig.handlesLock;
			this.failedPurgeAttempts = 0;
			this.fullCleanup = adjustedConfig.fullCleanup;
			if (adjustedConfig.getTtl === null) {
				this.getTtl = this._getTtl;
				this.basettl = adjustedConfig.ttl;
			} else {
				this.getTtl = adjustedConfig.getTtl;
			}
			this.evalFunction = adjustedConfig.evalFunction;
			this.expectedEval = adjustedConfig.expectedEval;
			this.purgeLoop = setInterval(this._purgeHandler.bind(this), adjustedConfig.cleanupInterval);
		} else {
			this.emit(LIB_EVENTS.BAD_CONFIG);
		}
	}

	static _adjustSettings(configuration) {
		const adjusted = { ...configuration };
		adjusted.cleanupInterval *= 1000;
		if (typeof configuration.ttl === 'function') {
			adjusted.getTtl = configuration.ttl;
		} else {
			adjusted.ttl = configuration.ttl;
			adjusted.getTtl = null;
		}
		if (typeof adjusted.onlyif === 'function') {
			adjusted.evalFunction = adjusted.onlyif;
			adjusted.expectedEval = true;
		} else {
			adjusted.evalFunction = adjusted.unless;
			adjusted.expectedEval = false;
		}
		return adjusted;
	}

	static isValidConfiguration(configuration) {
		if (typeof configuration !== 'object' || configuration === null) {
			return false;
		}
		return true;
	}

	_purgeHandler() {
		if (this.fullCleanup === true) {
			this.purge();
			this.emit(LIB_EVENTS.GC);
			return;
		}
		if (process._getActiveHandles().length < this.handlesLock || this.failedPurgeAttempts > 2) {
			// map id expiration by exp
			const now = Date.now();
			this.store.forEach((value, key) => {
				if (value.exp < now) {
					this.store.delete(key);
				}
			});
			this.failedPurgeAttempts = 0;
			this.emit(LIB_EVENTS.GC);
		} else {
			this.failedPurgeAttempts += 1;
		}
	}

	/**
	 * Returns the number of items
	 */
	size() {
		return this.store.size;
	}

	_getTtl() {
		return this.basettl;
	}

	/**
	 * Full cache eviction
	 */
	purge() {
		this.store.clear();
	}

	/**
	 * Stores an item as long as it doesn't meet the unless function
	 * @param {*} id String or number to be used as a key
	 * @param {*} object
	 */
	put(id, item) {
		// It won't override item if capacity is maxed out
		if (this.evalFunction(item) === this.expectedEval && this.store.size < this.limit) {
			const record = {
				exp: Date.now() + (this.getTtl(id, item) * 1000),
				item,
			};
			// schedule in deletion queue in case nobody tries to perform stale read
			this.store.set(id, record);
		}
	}

	get(id) {
		const record = this.store.get(id);
		if (typeof record === 'undefined') {
			this.emit(LIB_EVENTS.MISS);
			return null;
		}
		if (record.exp < Date.now()) {
			this.store.delete(id);
			this.emit(LIB_EVENTS.MISS_EXPIRED);
			return null;
		}
		this.emit(LIB_EVENTS.HIT);
		return record.item;
	}
}

exports.Cache = Cache;