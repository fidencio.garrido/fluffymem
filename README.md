# FluffyMem

## Simplistic key-value store

**Requires Node 8**

Use FluffyMem to store objects setting a TTL.

Features:
* In memory super simple key-value pair store
* Time to live set as a fixed value or through the usage of custom function
* Avoids performing cleanup when the program is busy

** Usage
```js
const { Cache } = require('fluffymem');


const cacheLarge = new Cache({
	limit: 800, // Store up to 800 items
	cleanupInterval: 30, // Time between forced cleanups in seconds
	ttl: 25, // Expiration in seconds
});

const customTTLCache = new Cache({
	limit: 20,
	cleanupInterval: 30, // Time between forced cleanups in seconds
	ttl: (id, item) => {
		if (id < 200000) {
			return 3;
		}
		return 50;
	},
	unless: (item) => {
		return (item === null); // Do not save if the item is null
	}
});

cacheLarge.put(1, 'me');
cacheLarge.put(2, { song: 'Smells like teen spirit' });

cacheLarge.on('hit', () => {
	console.log('They found something in the cache');
});

cacheLarge.on('miss', () => {
	console.log('Where are you item?!!!');
});

cacheLarge.on('miss_expired', () => {
	console.log('Technically still there, but cannot use it');
});

cacheLarge.on('gc', () => {
	console.log('I feel lighter');
};

customTTLCache.put(1, 'me');
customTTLCache.put(2, null); // will not be stored

```

## Using a reverse filter (onlyif) instead of unless
```js
const inclusiveCache = new Cache({
	limit: 20,
	cleanupInterval: 30, // Time between forced cleanups in seconds
	ttl: (id, item) => {
		if (id < 200000) {
			return 3;
		}
		return 50;
	},
	onlyif: (item) => {
		return (item < 1000); // Do not save if the item is null
	}
});

inclusiveCache.put(1, 1001);
inclusiveCache.put(2, 100);

console.log(inclusiveCache.get(1)); // null
console.log(inclusiveCache.get(2)); // 100
```

## Clearing the cache

```js
cache.purge();
```

## Configuration object

```js
const defaultConfiguration = {
	limit: 2000, // max number of items to keep
	cleanupInterval: 60, // seconds for gc execution
	handlesLock: 40, // if the number of node handles are higher than this, it will skip gc for up to two times
	ttl: 60, // document expiracy in seconds
	unless: () => false, // function to discard elements to put in the cache
	fullCleanup: false, // if true, it won't evaluate TTL for GC but rather will clean everything up
	onlyif: null, // reverse unless, it will storage only those elements who return true
};
```